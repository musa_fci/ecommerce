<?php
include '../vendor/autoload.php';

use App\classes\Model\Session;

Session::checkAdminSession();

use App\classes\Controller\CategoryController as CategoryController;
use App\classes\Controller\SubCategoryController as SubCategoryController;
use App\classes\Controller\BrandController as BrandController;
use App\classes\Controller\ProductController as ProductController;
use App\classes\Controller\OtherController as OtherController;
use App\classes\Model\Format as Format;

$conObj = new CategoryController;
$subConObj = new SubCategoryController;
$brandObj = new BrandController;
$pdObj = new ProductController;
$otherObj = new OtherController;
$format = new Format;
?>
<?php
if (isset($_GET['action']) && isset($_GET['action']) == 'logout') {
    Session::destroy();
    header("Location:login.php");
    exit();
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Admin</title>
        <meta name="description" content="Bootstrap Metro Dashboard">
        <meta name="author" content="Dennis Ji">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link id="bootstrap-style" href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap-responsive.min.css" rel="stylesheet">
        <link id="base-style" href="css/style.css" rel="stylesheet">
        <link id="base-style-responsive" href="css/style-responsive.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
        <link href="css/custom.css" type="text/css" rel="stylesheet">
        <script src="js/jquery-1.9.1.min.js"></script>

        <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
                <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
                <link id="ie-style" href="css/ie.css" rel="stylesheet">
        <![endif]-->

        <!--[if IE 9]>
                <link id="ie9style" href="css/ie9.css" rel="stylesheet">
        <![endif]-->

        <!-- start: Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico">
        <!-- end: Favicon -->

    </head>

    <body>

        <!-- start: Header -->
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="index.php"><span>Metro</span></a>

                    <!-- start: Header Menu -->
                    <div class="nav-no-collapse header-nav">
                        <ul class="nav pull-right">
                            <li class="dropdown hidden-phone">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="halflings-icon white warning-sign"></i>
                                </a>
                                <ul class="dropdown-menu notifications">
                                    <li class="dropdown-menu-title">
                                        <span>You have 11 notifications</span>
                                        <a href="#refresh"><i class="icon-repeat"></i></a>
                                    </li>	
                                    <li>
                                        <a href="#">
                                            <span class="icon blue"><i class="icon-user"></i></span>
                                            <span class="message">New user registration</span>
                                            <span class="time">1 min</span> 
                                        </a>
                                    </li>	
                                </ul>
                            </li>
                            <!-- start: Notifications Dropdown -->
                            <li class="dropdown hidden-phone">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="halflings-icon white tasks"></i>
                                </a>
                                <ul class="dropdown-menu tasks">
                                    <li class="dropdown-menu-title">
                                        <span>You have 17 tasks in progress</span>
                                        <a href="#refresh"><i class="icon-repeat"></i></a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <span class="header">
                                                <span class="title">iOS Development</span>
                                                <span class="percent"></span>
                                            </span>
                                            <div class="taskProgress progressSlim red">80</div> 
                                        </a>
                                    </li>                                

                                    <li>
                                        <a class="dropdown-menu-sub-footer">View all tasks</a>
                                    </li>	
                                </ul>
                            </li>
                            <!-- end: Notifications Dropdown -->
                            <!-- start: Message Dropdown -->
                            <li class="dropdown hidden-phone">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="halflings-icon white envelope"></i>
                                </a>
                                <ul class="dropdown-menu messages">
                                    <li class="dropdown-menu-title">
                                        <span>You have 9 messages</span>
                                        <a href="#refresh"><i class="icon-repeat"></i></a>
                                    </li>	

                                    <li>
                                        <a href="#">
                                            <span class="avatar"><img src="img/avatar.jpg" alt="Avatar"></span>
                                            <span class="header">
                                                <span class="from">
                                                    Dennis Ji
                                                </span>
                                                <span class="time">
                                                    Jul 25, 2012
                                                </span>
                                            </span>
                                            <span class="message">
                                                Lorem ipsum dolor sit amet consectetur adipiscing elit, et al commore
                                            </span>  
                                        </a>
                                    </li>
                                    <li>
                                        <a class="dropdown-menu-sub-footer">View all messages</a>
                                    </li>	
                                </ul>
                            </li>
                            <!-- end: Message Dropdown -->
                            <li>
                                <a class="btn" href="#">
                                    <i class="halflings-icon white wrench"></i>
                                </a>
                            </li>
                            <!-- start: User Dropdown -->
                            <li class="dropdown">
                                <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                                    <i class="halflings-icon white user"></i>
                                    <?php
                                    echo Session::get('username');
                                    ?>
                                    <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="dropdown-menu-title">
                                        <span>Account Settings</span>
                                    </li>
                                    <li><a href="#"><i class="halflings-icon user"></i> Profile</a></li>                                 
                                    <li>
                                        <a href="?action=logout"><i class="halflings-icon off"></i> Logout</a>
                                    </li>
                                </ul>
                            </li>
                            <!-- end: User Dropdown -->
                        </ul>
                    </div>
                    <!-- end: Header Menu -->

                </div>
            </div>
        </div>
        <!-- close Header -->
        <div class="container-fluid-full">
            <div class="row-fluid">

                <!-- start: Main Menu -->
                <div id="sidebar-left" class="span2">
                    <div class="nav-collapse sidebar-nav">
                        <ul class="nav nav-tabs nav-stacked main-menu">
                            <li>
                                <a href="?page=home">
                                    <i class="icon-bar-chart"></i>
                                    <span class="hidden-tablet"> Dashboard</span>
                                </a>
                            </li>
                            <li>
                                <a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Category</span></a>
                                <ul style="display: none;">
                                    <li><a class="submenu" href="?page=category"><i class="icon-file-alt"></i><span class="hidden-tablet"> Category Add</span></a></li>
                                    <li><a class="submenu" href="?page=category-list"><i class="icon-file-alt"></i><span class="hidden-tablet"> Category List</span></a></li>
                                    <li><a class="submenu" href="?page=sub-category"><i class="icon-file-alt"></i><span class="hidden-tablet"> Sub-Category Add</span></a></li>
                                    <li><a class="submenu" href="?page=sub-category-list"><i class="icon-file-alt"></i><span class="hidden-tablet"> Sub-Category List</span></a></li>
                                </ul>	
                            </li>

                            <li>
                                <a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Brand</span></a>
                                <ul style="display: none;">
                                    <li><a class="submenu" href="?page=brand"><i class="icon-file-alt"></i><span class="hidden-tablet"> Brand Add</span></a></li>
                                    <li><a class="submenu" href="?page=brand-list"><i class="icon-file-alt"></i><span class="hidden-tablet"> Brand List</span></a></li>
                                </ul>	
                            </li>

                            <li>
                                <a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Product </span></a>
                                <ul style="display: none;">
                                    <li><a class="submenu" href="?page=product"><i class="icon-file-alt"></i><span class="hidden-tablet"> Product Add</span></a></li>
                                    <li><a class="submenu" href="?page=product-list"><i class="icon-file-alt"></i><span class="hidden-tablet"> Product List</span></a></li>
                                </ul>	
                            </li>

                            <li>
                                <a class="dropmenu" href="#"><i class="icon-folder-close-alt"></i><span class="hidden-tablet"> Accessories </span></a>
                                <ul style="display: none;">
                                    <li><a class="submenu" href="?page=size"><i class="icon-file-alt"></i><span class="hidden-tablet"> Size Add</span></a></li>
                                    <li><a class="submenu" href="?page=color"><i class="icon-file-alt"></i><span class="hidden-tablet"> Color Add</span></a></li>
                                </ul>	
                            </li>

                            <!--                            <li>
                                                            <a href="?page=user"><i class="icon-user"></i>
                                                                <span class="hidden-tablet"> User</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="?page=add-blog">
                                                                <i class="icon-font"></i>
                                                                <span class="hidden-tablet"> Add Blog</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="?page=calendar">
                                                                <i class="icon-calendar"></i>
                                                                <span class="hidden-tablet"> Calendar</span>
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="?page=form">
                                                                <i class="icon-edit"></i>
                                                                <span class="hidden-tablet"> Forms</span>
                                                            </a>
                                                        </li>-->
                        </ul>
                    </div>
                </div>
                <!-- end: Main Menu -->


                <!-- start: Content -->
                <div id="content" class="span10">

                    <?php
                    if (isset($_GET['page'])) {
                        $page = 'pages/' . $_GET['page'] . '.php';
                        if (file_exists($page)) {
                            include $page;
                        } else {
                            include './pages/404.php';
                        }
                    } else {
                        include './pages/home.php';
                    }
                    ?>
                </div>
                <!-- end: Content -->



            </div>
        </div>




        <div class="clearfix"></div>
        <footer>
            <p>
                <span style="text-align:left;float:left">
                    &copy; <?php echo date('Y'); ?> 
                    <a href="">Bootstrap Metro Dashboard</a>
                </span>
            </p>
        </footer>

        <!-- start: JavaScript-->

        <script src="js/admin-Ajax.js"></script>
        <script src="js/jquery-migrate-1.0.0.min.js"></script>
        <script src="js/jquery-ui-1.10.0.custom.min.js"></script>
        <script src="js/jquery.ui.touch-punch.js"></script>
        <script src="js/modernizr.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.cookie.js"></script>
        <script src='js/fullcalendar.min.js'></script>
        <script src='js/jquery.dataTables.min.js'></script>
        <script src="js/excanvas.js"></script>
        <script src="js/jquery.flot.js"></script>
        <script src="js/jquery.flot.pie.js"></script>
        <script src="js/jquery.flot.stack.js"></script>
        <script src="js/jquery.flot.resize.min.js"></script>
        <script src="js/jquery.chosen.min.js"></script>
        <script src="js/jquery.uniform.min.js"></script>
        <script src="js/jquery.cleditor.min.js"></script>
        <script src="js/jquery.noty.js"></script>
        <script src="js/jquery.elfinder.min.js"></script>
        <script src="js/jquery.raty.min.js"></script>
        <script src="js/jquery.iphone.toggle.js"></script>
        <script src="js/jquery.uploadify-3.1.min.js"></script>
        <script src="js/jquery.gritter.min.js"></script>
        <script src="js/jquery.imagesloaded.js"></script>
        <script src="js/jquery.masonry.min.js"></script>
        <script src="js/jquery.knob.modified.js"></script>
        <script src="js/jquery.sparkline.min.js"></script>
        <script src="js/counter.js"></script>
        <script src="js/retina.js"></script>
        <script src="js/custom.js"></script>
        <!-- end: JavaScript-->

    </body>
</html>
