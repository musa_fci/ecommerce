<?php
if (isset($_GET['productId'])) {
    $productId = $_GET['productId'];
}
?>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $result = $pdObj->addProductWaysImages($_POST);
}
?>

<?php
//Product Ways Size Delete
if (isset($_GET['delid'])) {
    $delid = $_GET['delid'];
    $imageDelete = $pdObj->productWaysImageDelete($delid);

    if ($imageDelete) {
        echo "<script>setTimeout(function() {window.location = '?page=uploadImage&&productId=$productId'},1000);</script>";
    }
}
?>


<div class="row-fluid sortable">
    <?php
    if (isset($imageDelete)) {
        echo $imageDelete;
        unset($imageDelete);
    }
    ?>

    <?php
    if (isset($result)) {
        echo $result;
        unset($result);
    }
    ?>


    <div class="box span12">

        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Product Ways Image Upload From Here :</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>


        <div class="box-content">


            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="fileInput">Product Image</label>
                        <div class="controls">
                            <input type="hidden" name="product_id" value="<?php echo $productId ?>">
                            <input type="file" name="image[]" class="input-file uniform_on" id="fileInput">
                        </div>
                    </div>

                    <div class="control-group">
                        <div class="controls" id="rowinsert">

                        </div>
                    </div>
                    <div class="control-group">
                        <div class="controls">
                            <div id="addmore" style="font-size: 30px;" class="btn btn-info">+</div>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Upload Images</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   


        </div>
    </div>

    <div>
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Product Ways Images List</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="60%">Image</th>
                        <th width="30%">Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                    $imagelist = $pdObj->productWaysImageList($productId);
                    $i = 0;
                    if ($imagelist) {
                        foreach ($imagelist as $value) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center">
                                    <img src="<?php echo $value['image']; ?>" width="150px" height="150px" style="border: 2px solid green;">
                                </td>

                                <td class="center">
                                    <a class = "btn btn-danger" onclick = "return confirm('Are You Sure to Delete......!')" href = "?page=uploadImage&&productId=<?php echo $productId; ?>&&delid=<?php echo $value['id']; ?>">
                                        Delete
                                    </a>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>            
        </div>
    </div>

</div>