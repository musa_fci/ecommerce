<?php
if (isset($_GET['productEditId'])) {
    $productId = $_GET['productEditId'];
}
?>


<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $result = $pdObj->updateProduct($_POST, $productId);
}
?>


<div class="row-fluid sortable">
    <?php
    if (isset($result)) {
        echo $result;
        unset($result);
    }
    ?>
    <div class="box span12">

        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Edit a Product From Here :</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>


        <div class="box-content">
            <?php
            $data = $pdObj->productSelectById($productId);
            if ($data) {
                foreach ($data as $value) {
                    ?>

                    <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Product Name</label>
                                <div class="controls">
                                    <input type="text" name="product_name" value="<?php echo $value['product_name']; ?>" class="span6 typeahead" id="typeahead">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Category Name</label>
                                <div class="controls">
                                    <select name="cat_id" id="category">
                                        <option selected value="">--Category Name--</option>
                                        <?php
                                        $catData = $conObj->catList();
                                        if ($catData) {
                                            foreach ($catData as $catValue) {
                                                ?>
                                                <option
                                                <?php
                                                if ($value['cat_id'] == $catValue['id']) {
                                                    ?>
                                                        selected
                                                    <?php } ?>
                                                    value="<?php echo $catValue['id']; ?>"><?php echo $catValue['category_name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Sub Category Name</label>
                                <div class="controls">
                                    <select name="sub_cat_id" id="subCategory">
                                        <option selected value="">--Sub Category Name--</option>

                                        <?php
                                        $subcatData = $subConObj->subCategoryList();
                                        if ($subcatData) {
                                            foreach ($subcatData as $subcatValue) {
                                                ?>
                                                <option
                                                <?php
                                                if ($value['sub_cat_id'] == $subcatValue['sub_cat_id']) {
                                                    ?>
                                                        selected
                                                    <?php } ?>
                                                    value="<?php echo $subcatValue['sub_cat_id']; ?>"><?php echo $subcatValue['sub_cat_name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Brand Name</label>
                                <div class="controls">
                                    <select name="brand_id">
                                        <option selected value="">--Brand Name--</option>
                                        <?php
                                        $brandData = $brandObj->brandList();
                                        if ($brandData) {
                                            foreach ($brandData as $brandValue) {
                                                ?>
                                                <option 
                                                <?php
                                                if ($value['brand_id'] == $brandValue['id']) {
                                                    ?>
                                                        selected
                                                    <?php } ?>
                                                    value="<?php echo $brandValue['id']; ?>"><?php echo $brandValue['brand_name']; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group hidden-phone">
                                <label class="control-label" for="textarea2">Product Description</label>
                                <div class="controls">
                                    <textarea name="body" class="cleditor" id="textarea2" rows="3">
                                        <?php echo $value['body']; ?>
                                    </textarea>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Product price </label>
                                <div class="controls">
                                    <input type="text" name="price" value="<?php echo $value['price']; ?>" class="span6 typeahead" id="typeahead">
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label" for="typeahead">Quantity </label>
                                <div class="controls">
                                    <input type="text" name="quantity" value="<?php echo $value['quantity']; ?>" class="span6 typeahead" id="typeahead">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Product Code </label>
                                <div class="controls">
                                    <input type="text" name="product_code" value="<?php echo $value['product_code']; ?>" class="span6 typeahead" id="typeahead">
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Product Type</label>
                                <div class="controls">
                                    <select name="product_type">
                                        <option selected value="">--Product Type--</option>
                                        <?php
                                        if ($value['product_type'] == 'new') {
                                            ?>
                                            <option value="new" selected>New</option>
                                            <option value="hot">Hot</option>
                                            <option value="sell">Sell</option>
                                        <?php } elseif ($value['product_type'] == 'hot') {
                                            ?>
                                            <option value="new">New</option>
                                            <option value="hot" selected>Hot</option>
                                            <option value="sell">Sell</option>
                                        <?php } else {
                                            ?>
                                            <option value="new">New</option>
                                            <option value="hot">Hot</option>
                                            <option value="sell" selected>Sell</option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Product Discount(%)</label>
                                <div class="controls">
                                    <input type="text" name="discount" value="<?php echo $value['discount']; ?>" class="span6 typeahead" id="typeahead">
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-primary">Upload Product</button>
                                <button type="reset" class="btn">Cancel</button>
                            </div>
                        </fieldset>
                    </form>   
                    <?php
                }
            }
            ?>

        </div>
    </div>
</div>