
<?php
if (isset($_GET['catId'])) {
    $cat_id = $_GET['catId'];
}
?>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $result = $conObj->addCategoryWaysIcon($_POST);
}
?>

<?php
//Category Ways Icon Delete
if (isset($_GET['delid'])) {
    $delid = $_GET['delid'];
    $iconDelete = $conObj->categoryWaysIconDelete($delid);

    if ($iconDelete) {
        echo "<script>setTimeout(function() {window.location = '?page=catIconUpload&catId=$cat_id'},1000);</script>";
    }
}
?>


<div class="row-fluid sortable">

    <?php
    if (isset($iconDelete)) {
        echo $iconDelete;
        unset($iconDelete);
    }
    ?>

    <?php
    if (isset($result)) {
        echo $result;
        unset($result);
    }
    ?>


    <div class="box span12">

        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Product Ways Color Insert From Here :</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>


        <div class="box-content">


            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                <fieldset>
                    <div class="control-group">
                        <div class="controls">
                            <input type="hidden" name="cat_id" value="<?php echo $cat_id ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="selectError1">Category Icon</label>
                        <div class="controls">
                            <input type="file" name="cat_icon">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="selectError1">Category Name</label>
                        <div class="controls">
                            <input type="text" name="icon_name">
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Upload Icon</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   


        </div>
    </div>



    <div>
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Product Ways Color List</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="45%">Category Icon</th>
                        <th width="45%">Icon Name</th>
                        <th width="30%">Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                    $iconlist = $conObj->categoryWaysIconList($cat_id);
                    $i = 0;
                    if ($iconlist) {
                        foreach ($iconlist as $value) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center">
                                    <img src="<?php echo $value['cat_icon']; ?>" width="20px" height="20px">
                                </td>
                                <td class="center">
                                    <?php echo $value['icon_name']; ?>
                                </td>
                                <td class="center">
                                    <a class = "btn btn-danger" onclick = "return confirm('Are You Sure to Delete......!')" href = "?page=catIconUpload&catId=<?php echo $cat_id; ?>&&delid=<?php echo $value['icon_id']; ?>">
                                        Delete
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>            
        </div>
    </div>


</div>