<?php
//Sub Category Delete
if (isset($_GET['delid'])) {
    $delid = $_GET['delid'];
    $subCatDelete = $subConObj->deleteSubCat($delid);

    if ($subCatDelete) {
        echo "<script>setTimeout(function() {window.location = '?page=sub-category-list'},1000);</script>";
    }
}
?>

<?php
//Sub Category Disable
if (isset($_GET['disid'])) {
    $disid = $_GET['disid'];
    $subCatDisable = $subConObj->disableSubCategory($disid);
    if ($subCatDisable) {
        echo "<script>window.location = '?page=sub-category-list'</script>";
    }
}
?>

<?php
//Sub Category Enable
if (isset($_GET['enbid'])) {
    $enbid = $_GET['enbid'];
    $subCatEnable = $subConObj->enableSubCategory($enbid);

    if ($subCatEnable) {
        echo "<script>window.location = '?page=sub-category-list'</script>";
    }
}
?>


<div class="row-fluid sortable">

    <?php
    //Sub Category Delete Message
    if (isset($subCatDelete)) {
        echo $subCatDelete;
        unset($subCatDelete);
    }
    ?>

    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Sub Category List</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="30%">Category</th>
                        <th width="30%">Sub Category</th>
                        <th width="10%">Stats</th>
                        <th width="30%">Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                    $data = $subConObj->subCategoryList();
                    $i = 0;
                    if ($data) {
                        foreach ($data as $value) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center">
                                    <?php echo $value['category_name']; ?>
                                </td>
                                <td class="center"><?php echo $value['sub_cat_name']; ?></td>

                                <td class="center">
                                    <?php
                                    if ($value['status'] == 1) {
                                        ?>
                                        <span class="label label-success">Active</span>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="label label-danger">Inactive</span>
                                    <?php } ?>
                                </td>

                                <td class="center">
                                    <?php
                                    if ($value['status'] == 1) {
                                        ?>
                                        <a class = "btn btn-success" href = "?page=sub-category-list&disid=<?php echo $value['sub_cat_id']; ?>">
                                            Disable
                                        </a>
                                        <?php
                                    } else {
                                        ?>
                                        <a class = "btn btn-primary" href = "?page=sub-category-list&enbid=<?php echo $value['sub_cat_id']; ?>">
                                            Enable
                                        </a>

                                    <?php } ?>

                                    <a class = "btn btn-info" href = "?page=subCatEdit&subCatEditId=<?php echo $value['sub_cat_id']; ?>">
                                        Edit
                                    </a>
                                    <a class = "btn btn-danger" onclick = "return confirm('Are You Sure to Delete......!')" href = "?page=sub-category-list&delid=<?php echo $value['sub_cat_id']; ?>">
                                        Delete
                                    </a>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>            
        </div>
    </div>
</div>