<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $sizeadd = $otherObj->addSize($_POST);
}
?>

<?php
//Size Delete
if (isset($_GET['delid'])) {
    $delid = $_GET['delid'];
    $sizeDelete = $otherObj->deleteSize($delid);

    if ($sizeDelete) {
        echo "<script>setTimeout(function() {window.location = '?page=size'},1000);</script>";
    }
}
?>


<div class="row-fluid sortable">
    <?php
    if (isset($sizeadd)) {
        echo $sizeadd;
        unset($sizeadd);
    }
    ?>
    <?php
    if (isset($_SESSION['vError'])) {
        foreach ($_SESSION['vError'] as $error) {
            echo $error . '<br>';
        }
        unset($_SESSION['vError']);
    }
    ?>
    <div class="box span12">

        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add A New Size</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>


        <div class="box-content">
            <form action="" method="post" class="form-horizontal">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Size Name</label>
                        <div class="controls">
                            <input type="text" name="size_name" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Add A New Size</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   
        </div>
    </div>



    <div>
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Size List</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="45%">Size Name</th>
                        <th width="30%">Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                    $sizelist = $otherObj->sizeList();
                    $i = 0;
                    if ($sizelist) {
                        foreach ($sizelist as $value) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center"><?php echo $value['size_name']; ?></td>

                                <td class="center">
                                    <a class = "btn btn-danger" onclick = "return confirm('Are You Sure to Delete......!')" href = "?page=size&&delid=<?php echo $value['id']; ?>">
                                        Delete
                                    </a>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>            
        </div>
    </div>



</div>

