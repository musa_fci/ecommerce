<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $coloradd = $otherObj->addColor($_POST);
}
?>

<?php
//Size Delete
if (isset($_GET['delid'])) {
    $delid = $_GET['delid'];
    $colorDelete = $otherObj->deleteColor($delid);

    if ($colorDelete) {
        echo "<script>setTimeout(function() {window.location = '?page=color'},1000);</script>";
    }
}
?>


<div class="row-fluid sortable">
    <?php
    if (isset($coloradd)) {
        echo $coloradd;
        unset($coloradd);
    }
    ?>
    <?php
    if (isset($_SESSION['vError'])) {
        foreach ($_SESSION['vError'] as $error) {
            echo $error . '<br>';
        }
        unset($_SESSION['vError']);
    }
    ?>
    <div class="box span12">

        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add A New Color</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>


        <div class="box-content">
            <form action="" method="post" class="form-horizontal">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Color Name</label>
                        <div class="controls">
                            <input type="text" name="color_name" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Color Code</label>
                        <div class="controls">
                            <input type="color" name="color_code" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Add A New Color</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   
        </div>
    </div>




    <div class="box-header" data-original-title>
        <h2><i class="halflings-icon user"></i><span class="break"></span>Color List</h2>
        <div class="box-icon">
            <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
            <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
            <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
        </div>
    </div>
    <div class="box-content">
        <table class="table table-striped table-bordered bootstrap-datatable datatable">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th width="50%">Color & Color Name</th>
                    <th width="30%">Action</th>
                </tr>
            </thead>   
            <tbody>
                <?php
                $colorlist = $otherObj->colorList();
                $i = 0;
                if ($colorlist) {
                    foreach ($colorlist as $value) {
                        $i++;
                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td class="center">
                                <div style="width: 80px;padding:2px 20px;text-align:center;color:#EEE;background: <?php echo $value['color_code']; ?>">
                                    <?php echo $value['color_name']; ?>
                                </div>
                            </td>
                            <td class="center">
                                <a class = "btn btn-danger" onclick = "return confirm('Are You Sure to Delete......!')" href = "?page=color&&delid=<?php echo $value['id']; ?>">
                                    Delete
                                </a>
                            </td>

                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>            
    </div>




</div>

