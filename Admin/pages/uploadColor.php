
<?php
if (isset($_GET['productId'])) {
    $productId = $_GET['productId'];
}
?>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $result = $pdObj->addProductWaysColor($_POST);
}
?>

<?php
//Product Ways Color Delete
if (isset($_GET['delid'])) {
    $delid = $_GET['delid'];
    $colorDelete = $pdObj->productWaysColorDelete($delid);

    if ($colorDelete) {
        echo "<script>setTimeout(function() {window.location = '?page=uploadColor&&productId=$productId'},1000);</script>";
    }
}
?>


<div class="row-fluid sortable">

    <?php
    if (isset($colorDelete)) {
        echo $colorDelete;
        unset($colorDelete);
    }
    ?>

    <?php
    if (isset($result)) {
        echo $result;
        unset($result);
    }
    ?>


    <div class="box span12">

        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Product Ways Color Insert From Here :</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>


        <div class="box-content">


            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                <fieldset>
                    <div class="control-group">
                        <div class="controls">
                            <input type="hidden" name="product_id" value="<?php echo $productId ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="selectError1">Product Color</label>
                        <div class="controls">
                            <select id="selectError1" name="color_id[]" multiple data-rel="chosen">
                                <?php
                                $colorData = $otherObj->colorList();
                                if ($colorData) {
                                    foreach ($colorData as $value) {
                                        ?>
                                        <option value="<?php echo $value['id']; ?>" style="width: 100%;height:15px;background: <?php echo $value['color_code']; ?>">
                                            <?php echo $value['color_name']; ?>

                                        </option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Upload Color</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>


        </div>
    </div>



    <div>
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Product Ways Color List</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="45%">Color Name</th>
                        <th width="30%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $colorlist = $pdObj->productWaysColorList($productId);
                    $i = 0;
                    if ($colorlist) {
                        foreach ($colorlist as $value) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center">
                                  <div style="background:<?php echo $value['color_code']; ?>;width: 80px;padding: 2px 20px;text-align: center;color: #EEE;">
                                    <?php echo $value['color_name']; ?>
                                  </div>
                                </td>

                                <td class="center">
                                    <a class = "btn btn-danger" onclick = "return confirm('Are You Sure to Delete......!')" href = "?page=uploadColor&&productId=<?php echo $productId; ?>&&delid=<?php echo $value['id']; ?>">
                                        Delete
                                    </a>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>


</div>
