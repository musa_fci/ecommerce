<?php
if (isset($_GET['productId'])) {
    $productId = $_GET['productId'];
}
?>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $result = $pdObj->addProductWaysSize($_POST);
}
?>


<?php
//Product Ways Size Delete
if (isset($_GET['delid'])) {
    $delid = $_GET['delid'];
    $sizeDelete = $pdObj->productWaysSizeDelete($delid);

    if ($sizeDelete) {
        echo "<script>setTimeout(function() {window.location = '?page=uploadSize&&productId=$productId'},1000);</script>";
    }
}
?>


<div class="row-fluid sortable">

    <?php
    if (isset($sizeDelete)) {
        echo $sizeDelete;
        unset($sizeDelete);
    }
    ?>

    <?php
    if (isset($result)) {
        echo $result;
        unset($result);
    }
    ?>


    <div class="box span12">

        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Product Ways Size Insert From Here :</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>


        <div class="box-content">


            <form action="" method="post" class="form-horizontal">
                <fieldset>
                    <div class="control-group">
                        <div class="controls">
                            <input type="hidden" name="product_id" value="<?php echo $productId; ?>">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="selectError1">Product Size</label>
                        <div class="controls">
                            <select id="selectError1" name="size_id[]" multiple="" data-rel="chosen">
                                <?php
                                $sizeData = $otherObj->sizeList();
                                if ($sizeData) {
                                    foreach ($sizeData as $value) {
                                        ?>
                                        <option value="<?php echo $value['id']; ?>">
                                            <?php echo $value['size_name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Upload Size</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>


        </div>
    </div>



    <div>
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Product Ways Size List</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="45%">Size Name</th>
                        <th width="30%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $sizelist = $pdObj->productWaysSizeList($productId);
                    $i = 0;
                    if ($sizelist) {
                        foreach ($sizelist as $value) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center"><?php echo $value['size_name']; ?></td>

                                <td class="center">
                                    <a class = "btn btn-danger" onclick = "return confirm('Are You Sure to Delete......!')" href = "?page=uploadSize&&productId=<?php echo $productId; ?>&&delid=<?php echo $value['id']; ?>">
                                        Delete
                                    </a>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>
        </div>
    </div>

</div>
