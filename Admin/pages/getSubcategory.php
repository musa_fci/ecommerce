<?php
include '../../vendor/autoload.php';

use App\classes\Controller\SubCategoryController as SubCategoryController;

$subConObj = new SubCategoryController;

$catId = $_POST['catId'];
$subcatData = $subConObj->subcatListById($catId);

if ($subcatData) {
    foreach ($subcatData as $value) {
        ?>
        <option value="<?php echo $value['sub_cat_id']; ?>"><?php echo $value['sub_cat_name']; ?></option>
        <?php
    }
}
?>