<?php
//Sub Category Delete
if (isset($_GET['delid'])) {
    $delid = $_GET['delid'];
    $productDelete = $pdObj->deleteProduct($delid);

    if ($productDelete) {
        echo "<script>setTimeout(function() {window.location = '?page=product-list'},1000);</script>";
    }
}
?>

<?php
//Sub Category Disable
if (isset($_GET['disid'])) {
    $disid = $_GET['disid'];
    $productDisable = $pdObj->disableProduct($disid);
    if ($productDisable) {
        echo "<script>window.location = '?page=product-list'</script>";
    }
}
?>

<?php
//Sub Category Enable
if (isset($_GET['enbid'])) {
    $enbid = $_GET['enbid'];
    $productEnable = $pdObj->enableProduct($enbid);

    if ($productEnable) {
        echo "<script>window.location = '?page=product-list'</script>";
    }
}
?>


<div class="row-fluid sortable">

    <?php
    //Sub Category Delete Message
    if (isset($productDelete)) {
        echo $productDelete;
        unset($productDelete);
    }
    ?>

    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Product List</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="20%">P.Name</th>
                        <th width="5%">C.Name</th>
                        <th width="5%">SC.Name</th>
                        <th width="5%">B.Name</th>
                        <th width="20%">Body</th>
                        <th width="5%">Price</th>
                        <th width="20%">Image</th>
                        <th width="50%">Quantity</th>
                        <th width="5%">P.Code</th>
                        <th width="5%">P.Type</th>
                        <th width="5%">Size</th>
                        <th width="5%">Color</th>
                        <th width="5%">Discount(%)</th>
                        <th width="5%">Status</th>
                        <th width="30%">Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                    $data = $pdObj->productList();
                    $i = 0;
                    if ($data) {
                        foreach ($data as $value) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center"><?php echo $value['product_name']; ?></td>
                                <td class="center"><?php echo $value['category_name']; ?></td>
                                <td class="center"><?php echo $value['sub_cat_name']; ?></td>
                                <td class="center"><?php echo $value['brand_name']; ?></td>
                                <td class="center"><?php echo $format->textShort($value['body'], 80); ?></td>
                                <td class="center"><?php echo $value['price']; ?></td>
                                <td class="center">
                                    <a class = "btn btn-info" href = "?page=uploadImage&productId=<?php echo $value['product_id']; ?>">
                                        image
                                    </a>
                                </td>
                                <td class="center"><?php echo $value['quantity']; ?></td>
                                <td class="center"><?php echo $value['product_code']; ?></td>
                                <td class="center"><?php echo $value['product_type']; ?></td>
                                <td class="center">
                                    <a class = "btn btn-info" href = "?page=uploadSize&productId=<?php echo $value['product_id']; ?>">
                                        size
                                    </a>
                                </td>
                                <td class="center">
                                    <a class = "btn btn-info" href = "?page=uploadColor&productId=<?php echo $value['product_id']; ?>">
                                        color
                                    </a>
                                </td>
                                <td class="center"><?php echo $value['discount']; ?></td>

                                <td class="center">
                                    <?php
                                    if ($value['status'] == 1) {
                                        ?>
                                        <span class="label label-success">Active</span>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="label label-danger">Inactive</span>
                                    <?php } ?>
                                </td>

                                <td class="center">
                                    <?php
                                    if ($value['status'] == 1) {
                                        ?>
                                        <a class = "btn btn-success" href = "?page=product-list&disid=<?php echo $value['product_id']; ?>">
                                            Disable
                                        </a>
                                        <?php
                                    } else {
                                        ?>
                                        <a class = "btn btn-primary" href = "?page=product-list&enbid=<?php echo $value['product_id']; ?>">
                                            Enable
                                        </a>

                                    <?php } ?>

                                    <a class = "btn btn-info" href = "?page=productEdit&productEditId=<?php echo $value['product_id']; ?>">
                                        Edit
                                    </a>
                                    <a class = "btn btn-danger" onclick = "return confirm('Are You Sure to Delete......!')" href = "?page=product-list&delid=<?php echo $value['product_id']; ?>">
                                        Delete
                                    </a>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>            
        </div>
    </div>
</div>