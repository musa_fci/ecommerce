<?php
//Brand Delete
if (isset($_GET['delid'])) {
    $delid = $_GET['delid'];
    $brandDelete = $brandObj->deleteBrand($delid);

    if ($brandDelete) {
        echo "<script>setTimeout(function() {window.location = '?page=brand-list'},1000);</script>";
    }
}
?>

<?php
//Brand Disable
if (isset($_GET['disid'])) {
    $disid = $_GET['disid'];
    $brandDisable = $brandObj->disableCategory($disid);
    if ($brandDisable) {
        echo "<script>window.location = '?page=brand-list'</script>";
    }
}
?>

<?php
//Brand Enable
if (isset($_GET['enbid'])) {
    $enbid = $_GET['enbid'];
    $brandEnable = $brandObj->enableBrand($enbid);

    if ($brandEnable) {
        echo "<script>window.location = '?page=brand-list'</script>";
    }
}
?>


<div class="row-fluid sortable">

    <?php
    //Brand Delete Message
    if (isset($brandDelete)) {
        echo $brandDelete;
        unset($brandDelete);
    }
    ?>

    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Brand List</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="45%">Brand Name</th>
                        <th width="20%">Stats</th>
                        <th width="30%">Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                    $brandlist = $brandObj->brandList();
                    $i = 0;
                    if ($brandlist) {
                        foreach ($brandlist as $value) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center"><?php echo $value['brand_name']; ?></td>

                                <td class="center">
                                    <?php
                                    if ($value['status'] == 1) {
                                        ?>
                                        <span class="label label-success">Active</span>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="label label-danger">Inactive</span>
                                    <?php } ?>
                                </td>

                                <td class="center">
                                    <?php
                                    if ($value['status'] == 1) {
                                        ?>
                                        <a class = "btn btn-success" href = "?page=brand-list&disid=<?php echo $value['id']; ?>">
                                            Disable
                                        </a>
                                        <?php
                                    } else {
                                        ?>
                                        <a class = "btn btn-primary" href = "?page=brand-list&enbid=<?php echo $value['id']; ?>">
                                            Enable
                                        </a>

                                    <?php } ?>

                                    <a class = "btn btn-info" href = "?page=brandEdit&brandEditId=<?php echo $value['id']; ?>">
                                        Edit
                                    </a>
                                    <a class = "btn btn-danger" onclick = "return confirm('Are You Sure to Delete......!')" href = "?page=brand-list&delid=<?php echo $value['id']; ?>">
                                        Delete
                                    </a>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>            
        </div>
    </div>
</div>