<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $result = $pdObj->addProduct($_POST);
}
?>
<div class="row-fluid sortable">

    <?php
    if (isset($result)) {
        echo $result;
        unset($result);
    }
    ?>

    <?php
    if (isset($_SESSION['vError'])) {
        foreach ($_SESSION['vError'] as $error) {
            echo $error . '<br>';
        }
        unset($_SESSION['vError']);
    }
    ?>

    <div class="box span12">

        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add a New Product From Here :</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>


        <div class="box-content">


            <form action="" method="post" enctype="multipart/form-data" class="form-horizontal">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Product Name</label>
                        <div class="controls">
                            <input type="text" name="product_name" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Category Name</label>
                        <div class="controls">
                            <select name="cat_id" id="category">
                                <option selected value="">--Category Name--</option>
                                <?php
                                $catData = $conObj->catList();
                                if ($catData) {
                                    foreach ($catData as $value) {
                                        ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['category_name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Sub Category Name</label>
                        <div class="controls">
                            <select name="sub_cat_id" id="subCategory">
                                <option selected value="">--Sub Category Name--</option>

                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Brand Name</label>
                        <div class="controls">
                            <select name="brand_id">
                                <option selected value="">--Brand Name--</option>
                                <?php
                                $brandData = $brandObj->brandList();
                                if ($brandData) {
                                    foreach ($brandData as $value) {
                                        ?>
                                        <option value="<?php echo $value['id'];
                                        ?>"><?php echo $value['brand_name']; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group hidden-phone">
                        <label class="control-label" for="textarea2">Product Description</label>
                        <div class="controls">
                            <textarea name="body" class="cleditor" id="textarea2" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Product price </label>
                        <div class="controls">
                            <input type="text" name="price" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="typeahead">Product Quantity </label>
                        <div class="controls">
                            <input type="text" name="quantity" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Product Code </label>
                        <div class="controls">
                            <input type="text" name="product_code" class="span6 typeahead" id="typeahead">
                            <span>Last Code Was :
                                <?php
                                $lastProductCode = $pdObj->lastProductCode();
                                if ($lastProductCode) {
                                    foreach ($lastProductCode as $value) {
                                        echo $value['product_code'];
                                    }
                                }
                                ?>
                            </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Product Type</label>
                        <div class="controls">
                            <select name="product_type">
                                <option selected value="">--Product Type--</option>
                                <option value="new">New</option>
                                <option value="hot">Hot</option>
                                <option value="sell">Sell</option>
                            </select>
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="typeahead">Product Discount(%)</label>
                        <div class="controls">
                            <input type="text" name="discount" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Upload Product</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   


        </div>
    </div>
</div>