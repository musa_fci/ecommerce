<?php
if (isset($_GET['subCatEditId'])) {
    $subCatEditId = $_GET['subCatEditId'];
}
?>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $subCatId = $_POST['cat_id'];
    $subCatName = $_POST['sub_cat_name'];
    $catUpdate = $subConObj->updateSubCategory($subCatEditId, $subCatId, $subCatName);
}
?>

<div class="row-fluid sortable">

    <?php
    if (isset($catUpdate)) {
        echo $catUpdate;
        unset($catUpdate);
    }
    ?>
    <div class="box span12">

        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Update A Sub Category</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>


        <div class="box-content">
            <?php
            $SubCatSelect = $subConObj->subCategorySelectById($subCatEditId);
            if ($SubCatSelect) {
                foreach ($SubCatSelect as $value) {
                    ?>
                    <form action="" method="post" class="form-horizontal">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Category Name</label>
                                <div class="controls">
                                    <select name="cat_id">
                                        <?php
                                        $catlist = $conObj->catList();
                                        if ($catlist) {
                                            foreach ($catlist as $catValue) {
                                                ?>
                                                <option <?php if ($value['cat_id'] == $catValue['id']) { ?> selected="selected"<?php } ?> value="<?php echo $catValue['id'] ?>">
                                                    <?php echo $catValue['category_name']; ?>
                                                </option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Sub-Category Name</label>
                                <div class="controls">
                                    <input type="text" name="sub_cat_name" value="<?php echo $value['sub_cat_name']; ?>" class="span6 typeahead" id="typeahead">
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" name="btn" class="btn btn-primary">Update Sub-Category</button>
                                <button type="reset" class="btn">Cancel</button>
                            </div>
                        </fieldset>
                    </form> 
                    <?php
                }
            }
            ?>


        </div>


    </div>
</div>

