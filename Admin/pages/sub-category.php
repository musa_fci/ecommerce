<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
//    $catId = (isset($_POST['cat_id'])) ? $_POST['cat_id'] : ''; //if-else short form
    $subCatAdd = $subConObj->addSubCategory($_POST);
}
?>

<div class="row-fluid sortable">
    <?php
    if (isset($subCatAdd)) {
        echo $subCatAdd;
        unset($subCatAdd);
    }
    ?>
    <?php
    if (isset($_SESSION['vError'])) {
        foreach ($_SESSION['vError'] as $error) {
            echo $error . '<br>';
        }
        unset($_SESSION['vError']);
    }
    ?>
    <div class="box span12">

        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add A New Sub-Category</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>


        <div class="box-content">
            <form action="" method="post" class="form-horizontal">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Category Name</label>
                        <div class="controls">
                            <select name="cat_id">
                                <option selected value="">--Category Name--</option>
                                <?php
                                $catData = $conObj->catList();
                                if ($catData) {
                                    foreach ($catData as $value) {
                                        ?>
                                        <option value="<?php echo $value['id']; ?>"><?php echo $value['category_name']; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Sub-Category Name</label>
                        <div class="controls">
                            <input type="text" name="sub_cat_name" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Add A New Sub-Category</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   


        </div>
    </div>
</div>

