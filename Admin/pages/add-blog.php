<div class="row-fluid sortable">
    <div class="box span12">

        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Post a New Blog From Here :</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>


        <div class="box-content">


            <form class="form-horizontal">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="date01">Date input</label>
                        <div class="controls">
                            <input type="text" name="date" class="input-xlarge datepicker" id="date01" value="14/10/16">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="fileInput">Blog Images </label>
                        <div class="controls">
                            <input type="file" name="img" class="input-file uniform_on" id="fileInput">
                        </div>
                    </div>

                    <div class="control-group">
                        <label class="control-label" for="typeahead">Blog Title </label>
                        <div class="controls">
                            <input type="text" name="title" class="span6 typeahead" id="typeahead">
                        </div>
                    </div>

                    <div class="control-group hidden-phone">
                        <label class="control-label" for="textarea2">Blog Description</label>
                        <div class="controls">
                            <textarea class="cleditor" id="textarea2" rows="3"></textarea>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" name="btn" class="btn btn-primary">Blog Post</button>
                        <button type="reset" class="btn">Cancel</button>
                    </div>
                </fieldset>
            </form>   


        </div>
    </div>
</div>