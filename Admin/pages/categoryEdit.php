<?php
if (isset($_GET['catEditId'])) {
    $catEditId = $_GET['catEditId'];
}
?>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $catname = $_POST['category_name'];
    $catUpdate = $conObj->updateCategory($catEditId, $catname);
}
?>

<div class="row-fluid sortable">

    <?php
    if (isset($catUpdate)) {
        echo $catUpdate;
        unset($catUpdate);
    }
    ?>
    <div class="box span12">

        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add A New Category</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>


        <div class="box-content">
            <?php
            $catSelect = $conObj->categorySelectById($catEditId);
            if ($catSelect) {
                foreach ($catSelect as $value) {
                    ?>
                    <form action="" method="post" class="form-horizontal">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Category Name</label>
                                <div class="controls">
                                    <input type="text" name="category_name" value="<?php echo $value['category_name']; ?>" class="span6 typeahead" id="typeahead">
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" name="btn" class="btn btn-primary">Update Category</button>
                                <button type="reset" class="btn">Cancel</button>
                            </div>
                        </fieldset>
                    </form>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>

