<?php
//Category Delete
if (isset($_GET['delid'])) {
    $delid = $_GET['delid'];
    $catDelete = $conObj->deleteCategory($delid);

    if ($catDelete) {
        echo "<script>setTimeout(function() {window.location = '?page=category-list'},1000);</script>";
    }
}
?>

<?php
//Category Disable
if (isset($_GET['disid'])) {
    $disid = $_GET['disid'];
    $catDisable = $conObj->disableCategory($disid);

    if ($catDisable) {
        echo "<script>window.location = '?page=category-list'</script>";
    }
}
?>

<?php
//Category Enable
if (isset($_GET['enbid'])) {
    $enbid = $_GET['enbid'];
    $catEnable = $conObj->enableCategory($enbid);

    if ($catEnable) {
        echo "<script>window.location = '?page=category-list'</script>";
    }
}
?>


<div class="row-fluid sortable">

    <?php
    //Category Delete Message
    if (isset($catDelete)) {
        echo $catDelete;
        unset($catDelete);
    }
    ?>

    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Registered Members</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="45%">Category Name</th>
                        <th width="10%">Icon</th>
                        <th width="10%">Stats</th>
                        <th width="30%">Action</th>
                    </tr>
                </thead>   
                <tbody>
                    <?php
                    $catlist = $conObj->catList();
                    $i = 0;
                    if ($catlist) {
                        foreach ($catlist as $value) {
                            $i++;
                            ?>
                            <tr>
                                <td><?php echo $i; ?></td>
                                <td class="center"><?php echo $value['category_name']; ?></td>
                                <td class="center">
                                    <a class = "btn btn-info" href = "?page=catIconUpload&catId=<?php echo $value['id']; ?>">
                                        Icon
                                    </a>
                                </td>

                                <td class="center">
                                    <?php
                                    if ($value['status'] == 1) {
                                        ?>
                                        <span class="label label-success">Active</span>
                                        <?php
                                    } else {
                                        ?>
                                        <span class="label label-danger">Inactive</span>
                                    <?php } ?>
                                </td>

                                <td class="center">
                                    <?php
                                    if ($value['status'] == 1) {
                                        ?>
                                        <a class = "btn btn-success" href = "?page=category-list&disid=<?php echo $value['id']; ?>">
                                            Disable
                                        </a>
                                        <?php
                                    } else {
                                        ?>
                                        <a class = "btn btn-primary" href = "?page=category-list&enbid=<?php echo $value['id']; ?>">
                                            Enable
                                        </a>

                                    <?php } ?>

                                    <a class = "btn btn-info" href = "?page=categoryEdit&catEditId=<?php echo $value['id']; ?>">
                                        Edit
                                    </a>
                                    <a class = "btn btn-danger" onclick = "return confirm('Are You Sure to Delete......!')" href = "?page=category-list&delid=<?php echo $value['id']; ?>">
                                        Delete
                                    </a>
                                </td>

                            </tr>
                            <?php
                        }
                    }
                    ?>
                </tbody>
            </table>            
        </div>
    </div>
</div>