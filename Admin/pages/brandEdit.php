<?php
if (isset($_GET['brandEditId'])) {
    $brandEditId = $_GET['brandEditId'];
}
?>

<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $brandname = $_POST['brand_name'];
    $brandUpdate = $brandObj->updateBrand($brandEditId, $brandname);
}
?>

<div class="row-fluid sortable">

    <?php
    if (isset($brandUpdate)) {
        echo $brandUpdate;
        unset($brandUpdate);
    }
    ?>
    <div class="box span12">

        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon edit"></i><span class="break"></span>Add A New Category</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>


        <div class="box-content">
            <?php
            $brandSelect = $brandObj->brandSelectById($brandEditId);
            if ($brandSelect) {
                foreach ($brandSelect as $value) {
                    ?>
                    <form action="" method="post" class="form-horizontal">
                        <fieldset>
                            <div class="control-group">
                                <label class="control-label" for="typeahead">Category Name</label>
                                <div class="controls">
                                    <input type="text" name="brand_name" value="<?php echo $value['brand_name']; ?>" class="span6 typeahead" id="typeahead">
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" name="btn" class="btn btn-primary">Update Category</button>
                                <button type="reset" class="btn">Cancel</button>
                            </div>
                        </fieldset>
                    </form>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</div>

