<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header" data-original-title>
            <h2><i class="halflings-icon user"></i><span class="break"></span>Registered Members</h2>
            <div class="box-icon">
                <a href="#" class="btn-setting"><i class="halflings-icon wrench"></i></a>
                <a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
                <a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <table class="table table-striped table-bordered bootstrap-datatable datatable">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>E-Mail</th>
                        <th>Password</th>
                        <th>Birthday</th>
                        <th>Country</th>
                        <th>Address</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>   
                <tbody>
                    <tr>
                        <td>Md.Abu Musa</td>
                        <td class="center">musa@gmail.com</td>
                        <td class="center">pass123</td>
                        <td class="center">30-12-1222</td>
                        <td class="center">Bangladesh</td>
                        <td class="center">Feni,Sonagazi</td>
                        <td class="center">
                            <span class="label label-success">Active</span>
                        </td>
                        <td class="center">
                            <a class="btn btn-success" href="#">
                                <i class="halflings-icon white zoom-in"></i>  
                            </a>
                            <a class="btn btn-info" href="#">
                                <i class="halflings-icon white edit"></i>  
                            </a>
                            <a class="btn btn-danger" href="#">
                                <i class="halflings-icon white trash"></i> 
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td>Md.Abu Musa</td>
                        <td class="center">musa@gmail.com</td>
                        <td class="center">pass123</td>
                        <td class="center">30-12-1222</td>
                        <td class="center">Bangladesh</td>
                        <td class="center">Feni,Sonagazi</td>
                        <td class="center">
                            <span class="label label-warning">Pending</span>
                        </td>
                        <td class="center">
                            <a class="btn btn-success" href="#">
                                <i class="halflings-icon white zoom-in"></i>  
                            </a>
                            <a class="btn btn-info" href="#">
                                <i class="halflings-icon white edit"></i>  
                            </a>
                            <a class="btn btn-danger" href="#">
                                <i class="halflings-icon white trash"></i> 
                            </a>
                        </td>
                    </tr>

                    <tr>
                        <td>Md.Abu Musa</td>
                        <td class="center">musa@gmail.com</td>
                        <td class="center">pass123</td>
                        <td class="center">30-12-1222</td>
                        <td class="center">Bangladesh</td>
                        <td class="center">Feni,Sonagazi</td>
                        <td class="center">
                            <span class="label label-important">Banned</span>
                        </td>
                        <td class="center">
                            <a class="btn btn-success" href="#">
                                <i class="halflings-icon white zoom-in"></i>  
                            </a>
                            <a class="btn btn-info" href="#">
                                <i class="halflings-icon white edit"></i>  
                            </a>
                            <a class="btn btn-danger" href="#">
                                <i class="halflings-icon white trash"></i> 
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>            
        </div>
    </div>
</div>