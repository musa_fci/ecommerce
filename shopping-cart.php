<?php
include './inc/header.php';
?>

<?php
if($_SERVER['REQUEST_METHOD'] == 'POST'){
	echo $updateCart = $cartObj->updateCart($_POST);
}
?>

<?php
if(isset($_GET['product_id'])){
		$product_id = $_GET['product_id'];
		echo $deleteCart = $cartObj->deleteCart($product_id);
}
?>


<!-- ============================================== HEADER : END ============================================== -->
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="#">Home</a></li>
				<li class='active'>Shopping Cart</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content outer-top-xs">
	<div class="container">
		<div class="row ">
			<div class="shopping-cart">
				<div class="shopping-cart-table ">
	<div class="table-responsive">
		<table class="table">
			<thead>
				<tr>
					<th class="cart-romove item">Remove</th>
					<th class="cart-description item">Image</th>
					<th class="cart-product-name item">Product Name</th>
					<th class="cart-qty item">Quantity</th>
					<th class="cart-sub-total item">Subtotal</th>
					<th class="cart-total last-item">Grandtotal</th>
				</tr>
			</thead><!-- /thead -->

			<tbody>
				<?php
				$result = $cartObj->cartDataList();
				if($result){
					$sum = 0;
					foreach($result as $data){
				?>
				<form action="" method="post">
					<tr>
						<td class="romove-item">
							<a href="?product_id=<?php echo $data['product_id']; ?>" title="cancel" class="icon"><i class="fa fa-trash-o"></i></a>
						</td>
						<td class="cart-image">
							<a class="entry-thumbnail" href="detail.html">
							    <img src="Admin/<?php echo $data['image']; ?>" alt="">
							</a>
						</td>
						<td class="cart-product-name-info">
							<h4 class='cart-product-description'>
								<a href="detail.html"><?php echo $data['product_name']; ?></a>
							</h4>
							<div class="row">
								<div class="col-sm-4">
									<div class="rating rateit-small"></div>
								</div>
								<div class="col-sm-8">
									<div class="reviews">
										(06 Reviews)
									</div>
								</div>
							</div><!-- /.row -->

							<div class="cart-product-info">

								<span class="product-color">
									COLOR:
									<?php
										if($data['product_color']){
									?>
									<div style="height:30px;width:30px;background:<?php echo $data['product_color'];?>"></div>
									<?php
									}else{
									echo "NAN";
									}
									?>
								</span>

							</div>

						</td>
						<td class="cart-product-quantity">
							<div class="quant-input">
								<div class="cart-quantity">
										<div class="quant-input">
												<input type="number" name="quantity[]" value="<?php echo $data['quantity']; ?>">
										</div>
								</div>
	            </div>
	          </td>
						<td class="cart-product-sub-total"><span class="cart-sub-total-price">$<?php echo $data['product_price']; ?></span></td>
						<td class="cart-product-grand-total">
							<span class="cart-grand-total-price">$
								<?php
									echo $grandTotal = $data['product_price'] * $data['quantity'];
								 ?>
							</span>
						</td>
					</tr>
					<?php
						 $sum = $sum + $grandTotal;
					?>
				<?php }} ?>
			</tbody><!-- /tbody -->

			<tfoot>
				<tr>
					<td colspan="7">
						<div class="shopping-cart-btn">
							<span class="">
								<a href="index.php" class="btn btn-upper btn-primary outer-left-xs">Continue Shopping</a>
								<button type="submit" class="btn btn-upper btn-primary pull-right outer-right-xs">Update shopping cart</button>
							</span>
						</div><!-- /.shopping-cart-btn -->
					</td>
				</tr>
			</tfoot>
			</form>
		</table><!-- /table -->
	</div>
</div><!-- /.shopping-cart-table -->


<div class="col-md-4 col-sm-12 estimate-ship-tax">
	<table class="table">
		<thead>
			<tr>
				<th>
					<span class="estimate-title">Estimate shipping and tax</span>
					<p>Enter your destination to get shipping and tax.</p>
				</th>
			</tr>
		</thead><!-- /thead -->
		<tbody>
				<tr>
					<td>
						<div class="form-group">
							<label class="info-title control-label">Country <span>*</span></label>
							<select class="form-control unicase-form-control selectpicker">
								<option>--Select options--</option>
								<option>India</option>
								<option>SriLanka</option>
								<option>united kingdom</option>
								<option>saudi arabia</option>
								<option>united arab emirates</option>
							</select>
						</div>
						<div class="form-group">
							<label class="info-title control-label">State/Province <span>*</span></label>
							<select class="form-control unicase-form-control selectpicker">
								<option>--Select options--</option>
								<option>TamilNadu</option>
								<option>Kerala</option>
								<option>Andhra Pradesh</option>
								<option>Karnataka</option>
								<option>Madhya Pradesh</option>
							</select>
						</div>
						<div class="form-group">
							<label class="info-title control-label">Zip/Postal Code</label>
							<input type="text" class="form-control unicase-form-control text-input" placeholder="">
						</div>
						<div class="pull-right">
							<button type="submit" class="btn-upper btn btn-primary">GET A QOUTE</button>
						</div>
					</td>
				</tr>
		</tbody>
	</table>
</div><!-- /.estimate-ship-tax -->

<div class="col-md-4 col-sm-12 estimate-ship-tax">
	<table class="table">
		<thead>
			<tr>
				<th>
					<span class="estimate-title">Discount Code</span>
					<p>Enter your coupon code if you have one..</p>
				</th>
			</tr>
		</thead>
		<tbody>
				<tr>
					<td>
						<div class="form-group">
							<input type="text" class="form-control unicase-form-control text-input" placeholder="You Coupon..">
						</div>
						<div class="clearfix pull-right">
							<button type="submit" class="btn-upper btn btn-primary">APPLY COUPON</button>
						</div>
					</td>
				</tr>
		</tbody><!-- /tbody -->
	</table><!-- /table -->
</div><!-- /.estimate-ship-tax -->

<div class="col-md-4 col-sm-12 cart-shopping-total">
	<table class="table">
		<thead>
			<tr>
				<th>
					<div class="cart-sub-total">
						Subtotal<span class="inner-left-md">$ <?php if(isset($sum)){echo $sum;}else{echo "0";} ?></span>
					</div>
					<div class="cart-grand-total">
						Grand Total<span class="inner-left-md">$ <?php if(isset($sum)){echo $sum;}else{echo "0";} ?></span>
					</div>
				</th>
			</tr>
		</thead><!-- /thead -->
		<tbody>
				<tr>
					<td>
						<div class="cart-checkout-btn pull-right">
							<button type="submit" class="btn btn-primary checkout-btn">PROCCED TO CHEKOUT</button>
							<span class="">Checkout with multiples address!</span>
						</div>
					</td>
				</tr>
		</tbody><!-- /tbody -->
	</table><!-- /table -->
</div><!-- /.cart-shopping-total -->
</div><!-- /.shopping-cart -->
</div> <!-- /.row -->
		<!-- ============================================== BRANDS CAROUSEL ============================================== -->
<div id="brands-carousel" class="logo-slider wow fadeInUp">

		<div class="logo-slider-inner">
			<div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
				<div class="item m-t-15">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt="">
					</a>
				</div><!--/.item-->

				<div class="item m-t-10">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt="">
					</a>
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand3.png" src="assets/images/blank.gif" alt="">
					</a>
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt="">
					</a>
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt="">
					</a>
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand6.png" src="assets/images/blank.gif" alt="">
					</a>
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt="">
					</a>
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt="">
					</a>
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt="">
					</a>
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt="">
					</a>
				</div><!--/.item-->
		    </div><!-- /.owl-carousel #logo-slider -->
		</div><!-- /.logo-slider-inner -->

</div><!-- /.logo-slider -->
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div><!-- /.container -->
</div><!-- /.body-content -->



<!-- ============================================================= FOOTER ============================================================= -->
<?php
include './inc/footer.php';
?>

<!-- ============================================================= FOOTER : END============================================================= -->
