<?php

namespace App\classes\Controller;

use App\classes\Model\OtherModel;
use App\classes\Model\Format;

class OtherController {

    protected $model;
    protected $format;

    public function __construct() {
        $this->model = new OtherModel();
        $this->format = new Format();
    }

//Add Size
    public function addSize($data) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->addSize($fData);
        }
    }

//List of Size
    public function sizeList() {
        return $this->model->sizeList();
    }

//Delete a Size
    public function deleteSize($delid) {
        return $this->model->deleteSize($delid);
    }

    //Add Color
    public function addColor($data) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->addColor($fData);
        }
    }

//List of Color
    public function colorList() {
        return $this->model->colorList();
    }

//Delete a Color
    public function deleteColor($delid) {
        return $this->model->deleteColor($delid);
    }

}
