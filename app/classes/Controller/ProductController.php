<?php

namespace App\classes\Controller;

use App\classes\Model\ModelProduct;
use App\classes\Model\Format;

class ProductController {

    protected $model;
    protected $format;

    public function __construct() {
        $this->model  = new ModelProduct();
        $this->format = new Format();
    }

    //Add Product
    public function addProduct($data) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->addProduct($fData);
        }
    }

    //Product List
    public function productList() {
        return $this->model->productList();
    }

    //Last Product Code
    public function lastProductCode() {
        return $this->model->lastProductCode();
    }

    //Product Delete
    public function deleteProduct($delid) {
        return $this->model->deleteProduct($delid);
    }

    //Product Disable
    public function disableProduct($disid) {
        return $this->model->disableProduct($disid);
    }

    //Product Enable
    public function enableProduct($enbid) {
        return $this->model->enableProduct($enbid);
    }

    //Product Select By Id
    public function productSelectById($productId) {
        return $this->model->productSelectById($productId);
    }

    //Product Update
    public function updateProduct($data, $productId) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->updateProduct($fData, $productId);
        }
    }

    //Add Product Ways Images
    public function addProductWaysImages($data) {
        return $this->model->addProductWaysImages($data);
    }

    //Product Ways Images List
    public function productWaysImageList($productId) {
        return $this->model->productWaysImageList($productId);
    }

    //Product Ways Images Delete
    public function productWaysImageDelete($delid) {
        return $this->model->productWaysImageDelete($delid);
    }

    //Add product Ways Sizes
    public function addProductWaysSize($data) {
        return $this->model->addProductWaysSize($data);
    }

    //product Ways Size List
    public function productWaysSizeList($productId) {
        return $this->model->productWaysSizeList($productId);
    }

    //product Ways Size Delete
    public function productWaysSizeDelete($delid) {
        return $this->model->productWaysSizeDelete($delid);
    }

    //Add product Ways Colors
    public function addProductWaysColor($data) {
        return $this->model->addProductWaysColor($data);
    }

    //product Ways Color List
    public function productWaysColorList($productId) {
        return $this->model->productWaysColorList($productId);
    }

    //product Ways Color Delete
    public function productWaysColorDelete($delid) {
        return $this->model->productWaysColorDelete($delid);
    }

    //Get Product Details
    public function getProductDetails() {
        return $this->model->getProductDetails();
    }

    //Get Product Details
    public function getProductDetailsById($pid) {
        return $this->model->getProductDetailsById($pid);
    }

    //Get Product Ways Images By ID
    public function getProductWaysImgById($pid) {
        return $this->model->getProductWaysImgById($pid);
    }

    //Get Product Ways Sizes By ID
    public function getProductSizeById($pid) {
        return $this->model->getProductSizeById($pid);
    }

    //Get Product Ways Color By ID
    public function getProductColorById($pid) {
        return $this->model->getProductColorById($pid);
    }

}
