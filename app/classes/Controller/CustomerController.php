<?php

namespace App\classes\Controller;

use App\classes\Model\Session;
use App\classes\Model\ModelCustomer;
use App\classes\Model\Format;


class CustomerController{

  protected $model;
  protected $format;

  public function __construct() {
      $this->model  = new ModelCustomer();
      $this->format = new Format();
  }

//Add Customer
public function addCustomer($data){
    $fData = $this->format->filtering($data);
    $vData = $this->format->validation($data);
    if(count($vData) > 0){
      $_SESSION['vError'] = $vData;
    }else{
      return $this->model->addCustomer($fData);
    }
}


//Customer Login
public function loginCustomer($data){
    $fData = $this->format->filtering($data);
    $vData = $this->format->validation($data);
    if(count($vData) > 0){
      $_SESSION['vError'] = $vData;
    }else{
      return $this->model->loginCustomer($data);
    }
}






}
