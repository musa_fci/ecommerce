<?php

namespace App\classes\Controller;

use App\classes\Model\ModelSubCategory;
use App\classes\Model\Format;

class SubCategoryController {

    protected $model;
    protected $format;

    public function __construct() {
        $this->model = new ModelSubCategory();
        $this->format = new Format();
    }

//Sub Category Add or Insert
    public function addSubCategory($data) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->addSubCategory($fData);
        }
    }

//Sub Category Select
    public function subCategoryList() {
        return $this->model->subCategoryList();
    }

//Product List By Cat Id
    public function subcatListById($catId) {
        return $this->model->subcatListById($catId);
    }

//Sub Category Delete
    public function deleteSubCat($delid) {
        return $this->model->deleteSubCat($delid);
    }

//Sub Category Disable
    public function disableSubCategory($disid) {
        return $this->model->disableSubCategory($disid);
    }

//Sub Category Enable
    public function enableSubCategory($enbid) {
        return $this->model->enableSubCategory($enbid);
    }

//Sbu Category Select By ID
    public function subCategorySelectById($subCatEditId) {
        return $this->model->subCategorySelectById($subCatEditId);
    }

//Sub Category Update 
    public function updateSubCategory($subCatEditId, $subCatId, $subCatName) {
        return $this->model->updateSubCategory($subCatEditId, $subCatId, $subCatName);
    }

}
