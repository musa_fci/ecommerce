<?php

namespace App\classes\Controller;

use App\classes\Model\Session;
use App\classes\Model\ModelCategory;
use App\classes\Model\Format;

class CategoryController {

    protected $model;
    protected $format;

    public function __construct() {
        $this->model = new ModelCategory();
        $this->format = new Format();
    }

    //Category Add or Insert
    public function addCategory($data) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION['vError'] = $vData;
        } else {
            return $this->model->addCategory($fData);
        }
    }

    //Category Select
    public function catList() {
        return $this->model->catList();
    }

    //Category Select With Icon
    public function catListWithIcon() {
        return $this->model->catListWithIcon();
    }

    //Category Delete
    public function deleteCategory($delid) {
        return $this->model->deleteCategory($delid);
    }

    //Category Disable
    public function disableCategory($disid) {
        return $this->model->disableCategory($disid);
    }

    //Category Enable
    public function enableCategory($enbid) {
        return $this->model->enableCategory($enbid);
    }

    //Category Select By ID
    public function categorySelectById($catEditId) {
        return $this->model->categorySelectById($catEditId);
    }

    //Category Update
    public function updateCategory($catEditId, $catname) {
        return $this->model->updateCategory($catEditId, $catname);
    }

    //Category Ways Icon Upload
    public function addCategoryWaysIcon($data) {
        if ($data['icon_name'] == "") {
            return "<span class='error'>Field Must Not be Empty....!</span>";
        } else {
            return $this->model->addCategoryWaysIcon($data);
        }
    }

    //Category Ways Icon List
    public function categoryWaysIconList($cat_id) {
        return $this->model->categoryWaysIconList($cat_id);
    }

    //Category Ways Icon Delete
    public function categoryWaysIconDelete($delid) {
        return $this->model->categoryWaysIconDelete($delid);
    }

}
