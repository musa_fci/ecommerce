<?php

namespace App\classes\Controller;

use App\classes\Model\ModelCart;
use App\classes\Model\Format;

class CartController {

    protected $model;
    protected $format;

    public function __construct() {
        $this->model = new ModelCart();
        $this->format = new Format();
    }

    //Add Product To Cart
    public function addCart($data,$pid){
      $status = $this->model->checkSizeAndColor($data,$pid);
      if ($status === false) {
         return $this->model->addCart($data,$pid);
      }
      return $status;
    }

    //Select Cart Product List
    public function cartDataList(){
      return $this->model->cartDataList();
    }

    //Update Cart Productt
    public function updateCart($data){
      return $this->model->updateCart($data);
    }

    //Delete Cart Product
    public function deleteCart($product_id){
      return $this->model->deleteCart($product_id);
    }



}
