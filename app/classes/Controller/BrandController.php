<?php

namespace App\classes\Controller;

use App\classes\Model\ModelBrand;
use App\classes\Model\Format;

class BrandController {

    protected $model;
    protected $format;

    public function __construct() {
        $this->model = new ModelBrand();
        $this->format = new Format();
    }

    //Brand Add or Insert
    public function addBrand($data) {
        $fData = $this->format->filtering($data);
        $vData = $this->format->validation($data);
        if (count($vData) > 0) {
            $_SESSION["vError"] = $vData;
        } else {
            return $this->model->addBrand($fData);
        }
    }

    //Brand Select
    public function brandList() {
        return $this->model->brandList();
    }

    //Brand Delete
    public function deleteBrand($delid) {
        return $this->model->deleteBrand($delid);
    }

    //Brand Disable
    public function disableCategory($disid) {
        return $this->model->disableCategory($disid);
    }

    //Brand Enable
    public function enableBrand($enbid) {
        return $this->model->enableBrand($enbid);
    }

    //Brand Select By ID
    public function brandSelectById($brandEditId) {
        return $this->model->brandSelectById($brandEditId);
    }

    //Brand Update 
    public function updateBrand($brandEditId, $brandname) {
        return $this->model->updateBrand($brandEditId, $brandname);
    }

}
