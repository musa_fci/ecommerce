<?php

namespace App\classes\Model;

use App\classes\Model\Model;

class ModelSubCategory extends Model {

//Sub Category Add or Insert
    public function addSubCategory($data) {
        $query = "SELECT * FROM tbl_sub_category WHERE sub_cat_name = '$data[sub_cat_name]'";
        if ($this->db->select($query) == TRUE) {
            return "<span class='error'>Sub Category Already Added</span>";
            exit();
        } else {
            $queryr = "INSERT INTO tbl_sub_category (cat_id,sub_cat_name)VALUES('$data[cat_id]','$data[sub_cat_name]')";
            return $this->db->insert($queryr);
        }
    }

//Sub Category Select
    public function subCategoryList() {
        $query = "SELECT sc.*,c.category_name
                  FROM tbl_sub_category as sc,tbl_category as c
                  WHERE sc.cat_id = c.id ORDER BY sc.sub_cat_id DESC";
        return $this->db->select($query);
    }

//Product List By Cat Id
    public function subcatListById($catId) {
        $query = "SELECT * FROM tbl_sub_category WHERE cat_id = '$catId' AND status = '1'";
        return $this->db->select($query);
    }

//Sub Category Delete
    public function deleteSubCat($delid) {
        $query = "DELETE FROM tbl_sub_category WHERE sub_cat_id = '$delid'";
        return $this->db->delete($query);
    }

//Sub Category Disable
    public function disableSubCategory($disid) {
        $query = "UPDATE tbl_sub_category SET status = '0' WHERE sub_cat_id = '$disid'";
        return $this->db->update($query);
    }

//Sub Category Enable
    public function enableSubCategory($enbid) {
        $query = "UPDATE tbl_sub_category SET status = '1' WHERE sub_cat_id = '$enbid'";
        return $this->db->update($query);
    }

//Sub Category Select By ID
    public function subCategorySelectById($subCatEditId) {
        $query = "SELECT * FROM tbl_sub_category WHERE sub_cat_id = '$subCatEditId'";
        return $this->db->select($query);
    }

//Category Update
    function updateSubCategory($subCatEditId, $subCatId, $subCatName) {
        $query = "UPDATE tbl_sub_category SET sub_cat_name = '$subCatName',cat_id = '$subCatId' WHERE sub_cat_id = '$subCatEditId'";
        return $this->db->update($query);
    }

}
