<?php

namespace App\classes\Model;

use App\classes\Model\Model;

class ModelProduct extends Model {

//Add Product
    public function addProduct($data) {
        $query = "SELECT * FROM tbl_product WHERE product_code = '$data[product_code]'";
        if ($this->db->select($query)) {
            return "<span class='error'>Product Code Already Exist...!...</span>";
        } else {
            $price = $data['price'];
            $discount = $data['discount'];
            $discount_price = $price - ($price * $discount) / 100;
            $rquery = "INSERT INTO tbl_product (product_name,cat_id,sub_cat_id,brand_id,body,price,quantity,product_code,product_type,discount,discount_price)
          VALUES('$data[product_name]','$data[cat_id]','$data[sub_cat_id]','$data[brand_id]','$data[body]','$data[price]','$data[quantity]','$data[product_code]','$data[product_type]','$data[discount]','$discount_price')";
            return $this->db->insert($rquery);
        }
    }

//Product List
    public function productList() {
        $query = "SELECT p.*,c.category_name,sc.sub_cat_name,b.brand_name FROM tbl_product as p,tbl_category as c,tbl_sub_category as sc,tbl_brand as b
                WHERE p.cat_id = c.id AND p.sub_cat_id = sc.sub_cat_id AND p.brand_id = b.id ORDER BY p.product_id DESC";
        return $this->db->select($query);
    }

//Last Product Code
    public function lastProductCode() {
        $query = "SELECT * FROM tbl_product ORDER BY product_id DESC LIMIT 1";
        return $this->db->select($query);
    }

//Delete Product
    public function deleteProduct($delid) {
//        $query = "SELECT * FROM tbl_product WHERE product_id = '$delid'";
//        $result = $this->db->select($query);
//        foreach ($result as $value) {
//            $image = $value['image'];
//            unlink($image);
//        }
        $rquery = "DELETE FROM tbl_product WHERE product_id = '$delid'";
        return $this->db->delete($rquery);
    }

//Disable Product
    public function disableProduct($disid) {
        $query = "UPDATE tbl_product SET status = '0' WHERE product_id= '$disid'";
        return $this->db->update($query);
    }

//Enable Product
    public function enableProduct($enbid) {
        $query = "UPDATE tbl_product SET status = '1' WHERE product_id= '$enbid'";
        return $this->db->update($query);
    }

//Product Select By Id
    public function productSelectById($productId) {
        $query = "SELECT * FROM tbl_product WHERE product_id = '$productId'";
        return $this->db->select($query);
    }

//Product Update
    public function updateProduct($data, $productId) {

        $query = "UPDATE tbl_product SET product_name = '$data[product_name]',cat_id = '$data[cat_id]',sub_cat_id = '$data[sub_cat_id]',brand_id = '$data[brand_id]',body = '$data[body]',price = '$data[price]',quantity = '$data[quantity]',product_code = '$data[product_code]',product_type ='$data[product_type]',discount = '$data[discount]' WHERE product_id = '$productId'";
        return $this->db->update($query);

//        $permit = array("jpg", "jpeg", "png", "gif");
//        $file_name = $file['image']['name'];
//        $file_size = $file['image']['size'];
//        $tmp_name = $file['image']['tmp_name'];
//
//        $div = explode('.', $file_name);
//        $file_ext = strtolower(end($div));
//        $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
//        $uploaded_file = "uploads/" . $unique_image;
//
//        if (!empty($file['image']['name'])) {
//
//            $query = "SELECT * FROM tbl_product WHERE product_id = '$productId'";
//            $result = $this->db->select($query);
//            foreach ($result as $value) {
//                $image = $value['image'];
//                unlink($image);
//            }
//
//            move_uploaded_file($tmp_name, $uploaded_file);
//            $query = "UPDATE tbl_product SET product_name = '$data[product_name]',cat_id = '$data[cat_id]',sub_cat_id = '$data[sub_cat_id]',brand_id = '$data[brand_id]',body = '$data[body]',price = '$data[price]',image = '$uploaded_file',quantity = '$data[quantity]',product_code = '$data[product_code]',product_type ='$data[product_type]',discount = '$data[discount]' WHERE product_id = '$productId'";
//            return $this->db->update($query);
//        } else {
//            $query = "UPDATE tbl_product SET product_name = '$data[product_name]',cat_id = '$data[cat_id]',sub_cat_id = '$data[sub_cat_id]',brand_id = '$data[brand_id]',body = '$data[body]',price = '$data[price]',quantity = '$data[quantity]',product_code = '$data[product_code]',product_type ='$data[product_type]',discount = '$data[discount]' WHERE product_id = '$productId'";
//            return $this->db->update($query);
//        }
    }

    /*     * ************************************* */
    /*     * ******product Ways Images************ */
    /*     * ************************************* */

//Add Product Ways Images
    public function addProductWaysImages($data) {
        $status = null;
        foreach ($_FILES['image']['name'] as $key => $file) {
            $permit = array("jpg", "jpeg", "png", "gif");
            $file_name = $_FILES['image']['name'][$key];
            $file_size = $_FILES['image']['size'][$key];
            $tmp_name = $_FILES['image']['tmp_name'][$key];

            $div = explode('.', $file_name);
            $file_ext = strtolower(end($div));
            $unique_image = $key . substr(md5(time()), 0, 10) . '.' . $file_ext;
            $uploaded_file = "uploads/" . $unique_image;
            move_uploaded_file($tmp_name, $uploaded_file);

            $query = "INSERT INTO tbl_product_image(product_id,image)VALUES('$data[product_id]','$uploaded_file')";
            $status = $this->db->insert($query);
        }
        return $status;
    }

    //Product Ways Images List
    public function productWaysImageList($productId) {
        $query = "SELECT * FROM tbl_product_image WHERE product_id = '$productId'";
        return $this->db->select($query);
    }

    public function productWaysImageDelete($delid) {
        $query = "SELECT * FROM tbl_product_image WHERE id = '$delid'";
        $result = $this->db->select($query);
        foreach ($result as $value) {
            $image = $value['image'];
            unlink($image);
        }

        $rquery = "DELETE FROM tbl_product_image WHERE id = '$delid'";
        return $this->db->delete($rquery);
    }

    /*     * ************************************* */
    /*     * ******product Ways Sizes************ */
    /*     * ************************************* */


    //Add product Ways Sizes
    public function addProductWaysSize($data) {
      $status = null;
      foreach($data['size_id'] as $key => $value){
         $product_id = $data['product_id'];
         $size_id    = $data['size_id'][$key];

         $query = "SELECT * FROM tbl_product_size WHERE size_id = '$size_id' AND product_id = '$product_id'";
         if($this->db->select($query)){
           $status = "<span class='error'>Some Sizes Already Added</span>";
         }else{
           $query = "INSERT INTO tbl_product_size (product_id,size_id)VALUES('$product_id','$size_id')";
           $status = $this->db->insert($query);
         }

       }
       return $status;
    }



    //product Ways Size List
    public function productWaysSizeList($productId) {
        $query = "SELECT ps.*,s.size_name FROM tbl_product_size as ps,tbl_size as s
                WHERE ps.size_id = s.id AND product_id = '$productId' ORDER BY ps.id DESC";
        return $this->db->select($query);
    }

    //product Ways Size Delete
    public function productWaysSizeDelete($delid) {
        $query = "DELETE FROM tbl_product_size WHERE id = '$delid'";
        return $this->db->delete($query);
    }

    /*     * ************************************* */
    /*     * ******product Ways Colors************ */
    /*     * ************************************* */

    //Add product Ways Colors
    public function addProductWaysColor($data) {
      $status = null;
      foreach($data['color_id'] as $key => $value){
         $product_id = $data['product_id'];
         $color_id    = $data['color_id'][$key];

         $query = "SELECT * FROM tbl_product_color WHERE color_id = '$color_id' AND product_id = '$product_id'";
         if($this->db->select($query)){
           $status = "<span class='error'>Some Colors Already Added</span>";
         }else{
           $query = "INSERT INTO tbl_product_color (product_id,color_id)VALUES('$product_id','$color_id')";
           $status = $this->db->insert($query);
         }

       }
       return $status;
    }

    //product Ways Color List
    public function productWaysColorList($productId) {
        $query = "SELECT pc.*,c.* FROM tbl_product_color as pc,tbl_color as c
                WHERE pc.color_id = c.id AND product_id = '$productId' ORDER BY pc.id DESC";
        return $this->db->select($query);
    }

    //product Ways Size Delete
    public function productWaysColorDelete($delid) {
      // return $delid;
        $query = "DELETE FROM tbl_product_color WHERE color_id = '$delid'";
        return $this->db->delete($query);
    }

    //Get Product Details
    public function getProductDetails() {
        $query = "SELECT  p.*,pi.* FROM tbl_product as p, tbl_product_image as pi
                  WHERE p.product_id = pi.product_id AND pi.id = (SELECT id FROM tbl_product_image
                  WHERE product_id = pi.product_id  ORDER BY id ASC LIMIT 1) ORDER BY p.product_id DESC LIMIT 10";
        return $this->db->select($query);
    }

    //Get Product Details By ID
    public function getProductDetailsById($pid) {
        $query = "SELECT * FROM tbl_product WHERE product_id = '$pid'";
        return $this->db->select($query);
    }

    //Get Product Ways Images By ID
    public function getProductWaysImgById($pid) {
        $query = "SELECT * FROM tbl_product_image WHERE product_id = '$pid'";
        return $this->db->select($query);
    }

    //Get Product Ways Sizes By ID
    public function getProductSizeById($pid) {
      $query = "SELECT ps.*,s.* FROM tbl_product_size as ps, tbl_size as s WHERE ps.product_id = '$pid' AND ps.size_id=s.id";
      return $this->db->select($query);
    }


    //Get Product Ways Colors By ID
    public function getProductColorById($pid) {
      $query = "SELECT pc.*,c.* FROM tbl_product_color as pc, tbl_color as c WHERE pc.product_id = '$pid' AND pc.color_id=c.id";
      return $this->db->select($query);
    }








}
