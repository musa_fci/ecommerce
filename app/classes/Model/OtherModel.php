<?php

namespace App\classes\Model;

use App\classes\Model\Model;

class OtherModel extends Model {

    //Add Size
    public function addSize($data) {
        $query = "SELECT * FROM tbl_size WHERE size_name = '$data[size_name]'";
        if ($this->db->select($query) == TRUE) {
            return "<span class='error'>Size Already Added</span>";
            exit();
        } else {
            $queryr = "INSERT INTO tbl_size (size_name)VALUES('$data[size_name]')";
            return $this->db->insert($queryr);
        }
    }

    //List of Size
    public function sizeList() {
        $query = "SELECT * FROM tbl_size ORDER BY id DESC";
        return $this->db->select($query);
    }

    //Delete a Size

    public function deleteSize($delid) {
        $query = "DELETE FROM tbl_size WHERE id = '$delid'";
        return $this->db->delete($query);
    }

    //Add Size
    public function addColor($data) {
        $query = "SELECT * FROM tbl_color WHERE color_name = '$data[color_name]'";
        if ($this->db->select($query) == TRUE) {
            return "<span class='error'>Color Already Added</span>";
            exit();
        } else {
            $queryr = "INSERT INTO tbl_color (color_name,color_code)VALUES('$data[color_name]','$data[color_code]')";
            return $this->db->insert($queryr);
        }
    }

    //List of Size
    public function colorList() {
        $query = "SELECT * FROM tbl_color ORDER BY id DESC";
        return $this->db->select($query);
    }

    //Delete a Size

    public function deleteColor($delid) {
        $query = "DELETE FROM tbl_color WHERE id = '$delid'";
        return $this->db->delete($query);
    }

}
