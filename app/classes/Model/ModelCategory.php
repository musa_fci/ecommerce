<?php

namespace App\classes\Model;

use App\classes\Model\Model;

class ModelCategory extends Model {

    //Category Add or Insert
    public function addCategory($data) {
        $query = "SELECT * FROM tbl_category WHERE category_name = '$data[category_name]'";
        if ($this->db->select($query) == TRUE) {
            return "<span class='error'>Category Already Added</span>";
            exit();
        } else {
            $queryr = "INSERT INTO tbl_category (category_name)VALUES('$data[category_name]')";
            return $this->db->insert($queryr);
        }
    }

    //Category Select
    public function catList() {
        $query = "SELECT * FROM tbl_category ORDER BY id ASC";
        return $this->db->select($query);
    }

    //Category Select With Icon
    public function catListWithIcon() {
        $query = "SELECT c.*,ci.* FROM tbl_category as c,tbl_category_icon as ci WHERE c.id = ci.cat_id AND c.status = '1' ORDER BY c.id ASC";
        return $this->db->select($query);
    }

    //Category Delete
    public function deleteCategory($delid) {
        $query = "DELETE FROM tbl_category WHERE id = '$delid'";
        return $this->db->delete($query);
    }

    //Category Disable
    public function disableCategory($disid) {
        $query = "UPDATE tbl_category SET status = '0' WHERE id = '$disid'";
        return $this->db->update($query);
    }

    //Category Enable
    public function enableCategory($enbid) {
        $query = "UPDATE tbl_category SET status = '1' WHERE id = '$enbid'";
        return $this->db->update($query);
    }

    //Category Select By ID
    public function categorySelectById($catEditId) {
        $query = "SELECT * FROM tbl_category WHERE id = '$catEditId'";
        return $this->db->select($query);
    }

    //Category Update
    function updateCategory($catEditId, $catname) {
        $query = "UPDATE tbl_category SET category_name = '$catname' WHERE id = '$catEditId'";
        return $this->db->update($query);
    }

    //Category Ways Icon Upload
    public function addCategoryWaysIcon($data = NULL) {
        $rquery = "SELECT * FROM tbl_category_icon WHERE cat_id = '$data[cat_id]'";
        if ($this->db->select($rquery) == TRUE) {
            echo "<span class='error'>One Icon Already Uploaded For This Category....!</span>";
        } else {
            $permit = array("jpg", "jpeg", "png", "gif");
            $file_name = $_FILES['cat_icon']['name'];
            $file_size = $_FILES['cat_icon']['size'];
            $tmp_name = $_FILES['cat_icon']['tmp_name'];

            $div = explode('.', $file_name);
            $file_ext = strtolower(end($div));
            $unique_image = substr(md5(time()), 0, 10) . '.' . $file_ext;
            $uploaded_file = "uploads/icon/" . $unique_image;
            move_uploaded_file($tmp_name, $uploaded_file);

            $query = "INSERT INTO tbl_category_icon(cat_id,cat_icon,icon_name)VALUES('$data[cat_id]','$uploaded_file','$data[icon_name]')";
            return $this->db->insert($query);
        }
    }

    //Category Ways Icon List
    public function categoryWaysIconList($cat_id) {
        $query = "SELECT * FROM tbl_category_icon WHERE cat_id = '$cat_id'";
        return $this->db->select($query);
    }

    //Category Ways Icon Delete
    public function categoryWaysIconDelete($delid) {
        $query = "SELECT * FROM tbl_category_icon WHERE icon_id = '$delid'";
        $result = $this->db->select($query);
        if ($result) {
            foreach ($result as $value) {
                $icon = $value['cat_icon'];
                unlink($icon);
            }
        }

        $rquery = "DELETE FROM tbl_category_icon WHERE icon_id = '$delid'";
        return $this->db->delete($rquery);
    }

}
