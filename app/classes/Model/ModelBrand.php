<?php

namespace App\classes\Model;

use App\classes\Model\Model;

class ModelBrand extends Model {

//Brand Add or Insert
    public function addBrand($data) {
        $query = "SELECT * FROM tbl_brand WHERE brand_name = '$data[brand_name]'";
        if ($this->db->select($query) == TRUE) {
            return "<span class='error'>Brand Already Added</span>";
            exit();
        } else {
            $queryr = "INSERT INTO tbl_brand (brand_name)VALUES('$data[brand_name]')";
            return $this->db->insert($queryr);
        }
    }

//Brand Select
    public function brandList() {
        $query = "SELECT * FROM tbl_brand ORDER BY id DESC";
        return $this->db->select($query);
    }

//Brand Delete
    public function deleteBrand($delid) {
        $query = "DELETE FROM tbl_brand WHERE id = '$delid'";
        return $this->db->delete($query);
    }

//Category Disable
    public function disableCategory($disid) {
        $query = "UPDATE tbl_brand SET status = '0' WHERE id = '$disid'";
        return $this->db->update($query);
    }

//Brand Enable
    public function enableBrand($enbid) {
        $query = "UPDATE tbl_brand SET status = '1' WHERE id = '$enbid'";
        return $this->db->update($query);
    }

//Brand Select By ID
    public function brandSelectById($brandEditId) {
        $query = "SELECT * FROM tbl_brand WHERE id = '$brandEditId'";
        return $this->db->select($query);
    }

//Brand Update
    function updateBrand($brandEditId, $brandname) {
        $query = "UPDATE tbl_brand SET brand_name = '$brandname' WHERE id = '$brandEditId'";
        return $this->db->update($query);
    }

}
