<?php

namespace App\classes\Model;

use App\classes\Model\Model;

class ModelCart extends Model {

  //Check Product Color and Size
  public function checkSizeAndColor($data,$pid){

      $sizeColor = $color = $size = 0;

      if (! isset($data['product_size']) && ! isset($data['product_color'])) {
          $query = "select * from tbl_product as p inner join tbl_product_size as s on p.product_id = s.product_id inner join tbl_product_color as c on p.product_id = c.product_id where p.product_id = '$pid'";
          $sizeColor = $this->db->select($query);
      }elseif (! isset($data['product_size'])) {
          $query = "select * from tbl_product as p inner join tbl_product_size as s on p.product_id = s.product_id where p.product_id = '$pid'";
          $size = $this->db->select($query);
      }elseif (! isset($data['product_color'])) {
          $query = "select * from tbl_product as p inner join tbl_product_color as c on p.product_id = c.product_id where p.product_id = '$pid'";
          $color = $this->db->select($query);
      }


      if ( isset($sizeColor->num_rows) && $sizeColor->num_rows > 0) {
        return "Size And Color Not Select.";
      }elseif (isset($size->num_rows) && $size->num_rows > 0) {
        return "Size Not Select.";
      }elseif (isset($color->num_rows) && $color->num_rows > 0) {
        return "Color Not Select.";
      }else{
        return false;
      }

  }

  //Product Add to Cart
  public function addCart($data,$pid){
    $sId = session_id();
    $quantity = $data['quantity'];

    if($quantity <= 0){
      return "Please Select Product Quantity";
    }else{

    $checkQuery = "SELECT * FROM tbl_cart WHERE product_id = '$pid' AND session_id = '$sId'";
    if($this->db->select($checkQuery)){
      return "Product Already Added";
    }else {

    $query = "SELECT * FROM tbl_product WHERE product_id = '$pid'";
    $result = $this->db->select($query)->fetch_assoc();

    $totalProductQuantity  = $result['quantity'];
    $remainProductQuantity = ($totalProductQuantity - $quantity);

    $query = "UPDATE tbl_product SET quantity = '$remainProductQuantity' WHERE product_id = '$pid'";
    $this->db->update($query);

    $productName = $result['product_name'];
    if($result['discount_price'] == ""){
      $productPrice = $result['price'];
    }else{
    $productPrice = $result['discount_price'];
    }

    $imgQuery = "SELECT * FROM tbl_product_image WHERE product_id = '$pid' ORDER BY id ASC LIMIT 1";
    $imgResult = $this->db->select($imgQuery)->fetch_assoc();
    $image = $imgResult['image'];

    if (! isset($data['product_size'])) {
      $rquery = "INSERT INTO tbl_cart(session_id,product_id,product_name,quantity,product_price,product_color,image)VALUES('$sId','$pid','$productName','$data[quantity]','$productPrice','$data[product_color]','$image')";
      return $this->db->insert($rquery);
    }elseif (! isset($data['product_color'])) {
      $rquery = "INSERT INTO tbl_cart(session_id,product_id,product_name,quantity,product_price,product_size,image)VALUES('$sId','$pid','$productName','$data[quantity]','$productPrice','$data[product_size]','$image')";
      return $this->db->insert($rquery);
    }elseif (! isset($data['product_size']) && ! isset($data['product_color'])) {
      $rquery = "INSERT INTO tbl_cart(session_id,product_id,product_name,quantity,product_price,image)VALUES('$sId','$pid','$productName','$data[quantity]','$productPrice','$image')";
      return $this->db->insert($rquery);
    }
    else{
      $rquery = "INSERT INTO tbl_cart(session_id,product_id,product_name,quantity,product_price,product_size,product_color,image)VALUES('$sId','$pid','$productName','$data[quantity]','$productPrice','$data[product_size]','$data[product_color]','$image')";
      return $this->db->insert($rquery);
    }
  }
}
}


//Select Cart Product List
public function cartDataList(){
  $sId = session_id();
  $query = "SELECT * FROM tbl_cart WHERE session_id = '$sId'";
  return $this->db->select($query);
}


//Update Cart Product
public function updateCart($data)
{
    $status = null;
    foreach($data['product_id'] as $key => $value){
       $product_id = $data['product_id'][$key];
       $quantity  = $data['quantity'][$key];

       $query = "SELECT * FROM tbl_cart WHERE product_id = '$product_id'";
       $result = $this->db->select($query)->fetch_assoc();
       $cppq = $result['quantity'];  //cppq = cart present product quantity

   if($quantity <= 0){
       return "Please Select Product Quantity";
   }elseif($quantity >= $cppq){
      $updateQuantity = $quantity - $cppq;
      $query = "UPDATE tbl_cart SET quantity = '$quantity' WHERE product_id = '$product_id'";
      $status = $this->db->update($query);

      $query = "SELECT * FROM tbl_product WHERE product_id = '$product_id'";
      $result = $this->db->select($query)->fetch_assoc();
      $ptpq = $result['quantity']; //Product table present quantity
      $totalProduct = $ptpq - $updateQuantity;

      $query = "UPDATE tbl_product SET quantity = '$totalProduct' WHERE product_id = '$product_id' ";
      $this->db->update($query);

   }elseif($quantity <= $cppq){
     $updateQuantity = $cppq - $quantity;
     $query = "UPDATE tbl_cart SET quantity = '$quantity' WHERE product_id = '$product_id'";
     $status = $this->db->update($query);

     $query = "SELECT * FROM tbl_product WHERE product_id = '$product_id'";
     $result = $this->db->select($query)->fetch_assoc();
     $ptpq = $result['quantity']; //Product table present quantity
     $totalProduct = $ptpq + $updateQuantity;

     $query = "UPDATE tbl_product SET quantity = '$totalProduct' WHERE product_id = '$product_id' ";
     $this->db->update($query);

   }
}
    return $status;
}


//Delete Cart Product
public function deleteCart($product_id){

  $query = "SELECT * FROM tbl_cart WHERE product_id = '$product_id'";
  $result = $this->db->select($query)->fetch_assoc();
  $cppq = $result['quantity'];  //cppq = cart present product quantity


  $query = "SELECT * FROM tbl_product WHERE product_id = '$product_id'";
  $result = $this->db->select($query)->fetch_assoc();
  $ptpq = $result['quantity']; //Product table present quantity
  $totalProduct = $ptpq + $cppq;

  $query = "UPDATE tbl_product SET quantity = '$totalProduct' WHERE product_id = '$product_id' ";
  $this->db->update($query);

  $query  = "DELETE FROM tbl_cart WHERE product_id = '$product_id'";
  $this->db->delete($query);
  return "<script>window.location = 'shopping-cart.php';</script>";

}


}
