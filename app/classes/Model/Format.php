<?php

namespace App\classes\Model;

class Format {

    //Data Filtering
    public function filtering($data = null) {
      $filterData = [];
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                $filterData[$key] = trim($value);
                $filterData[$key] = stripslashes($value);
                $filterData[$key] = htmlspecialchars($value);
            }
            $data = $filterData;
        } else {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
        }
        return $data;
    }

    //Data Validation
    public function validation($data = null) {
        $error = array();
        foreach ($data as $key => $value) {
            if (empty($value)) {
                $error[] = "<span class ='error'>" . ucfirst($key) . " Field Must Not Be Empty.</span>";
            }
        }
        return $error;
    }

    //Text Short
    public function textShort($data, $limit = 60) {
        $text = substr($data, 0, $limit);
        $text = substr($data, 0, strrpos($text, ' ')) . '...'; //use strrpos for prevent word broken
        return $text;
    }

}
