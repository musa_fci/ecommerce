<div class="side-menu animate-dropdown outer-bottom-xs">
    <div class="head"><i class="icon fa fa-align-justify fa-fw"></i> Categories</div>
    <nav class="yamm megamenu-horizontal" role="navigation">
        <?php
        $cat = $conObj->catListWithIcon();
        if ($cat) {
            foreach ($cat as $data) {
                ?>
                <ul class="nav">
                    <li class="dropdown menu-item">
                        <a href="category.php?id=<?php echo $data['id']; ?>" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="<?php echo $data['icon_name']; ?>" aria-hidden="true"></i>
                            <?php echo $data['category_name']; ?>
                        </a>

                        <?php
                       $catId = $data['id'];
                       $subcat = $subConObj->subcatListById($catId);
                       if (empty($subcat)) {
                           echo "";
                       } else {
                            ?>


                            <ul class="dropdown-menu mega-menu">
                                <li class="yamm-content">
                                    <div class="row">
                                        <div class="col-sm-12 col-md-3">
                                            <ul class="links list-unstyled">
                                                <?php
                                                $catId = $data['id'];
                                                $i = 0;
                                                $subcat = $subConObj->subcatListById($catId);
                                                if ($subcat) {
                                                    foreach ($subcat as $idata) {
                                                        $i++;
                                                        ?>
                                                        <li><a href="category.php?id=<?php echo $idata['sub_cat_id']; ?>"><?php echo $idata['sub_cat_name']; ?></a></li>
                                                        <?php
                                                        if ($i == 3) {
                                                            ?>
                                                        </ul>
                                                    </div>
                                                    <div class="col-sm-12 col-md-3">
                                                        <ul class="links list-unstyled">
                                                            <?php
                                                            $i = 0;
                                                        }
                                                    }
                                                }
                                                ?>
                                            </ul>
                                        </div><!-- /.col -->
                                    </div><!-- /.row -->
                                </li><!-- /.yamm-content -->
                            </ul><!-- /.dropdown-menu -->


                            <?php
                       }
                        ?>


                    </li><!-- /.menu-item -->
                </ul><!-- /.nav -->
                <?php
            }
        }
        ?>
    </nav><!-- /.megamenu-horizontal -->
</div><!-- /.side-menu -->
